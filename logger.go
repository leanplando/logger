package logger

import (
	"bitbucket.org/leanplando/logger/internal"
	"context"
	"github.com/sirupsen/logrus"
	"io"
	"os"
)

var level logrus.Level = logrus.TraceLevel

func SetLevel(l logrus.Level) {
	level = l
}

func WithContext(ctx context.Context) *logrus.Entry {
	l := logrus.StandardLogger()

	l.SetFormatter(internal.ContextFormatter{})
	l.SetReportCaller(true)
	l.SetLevel(logrus.TraceLevel)

	if env := os.Getenv("env"); env == "TEST" {
		l.SetOutput(io.Discard)
	}

	return l.WithContext(ctx)
}
