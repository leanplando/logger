package internal

import (
	"github.com/newrelic/go-agent/v3/integrations/logcontext"
	"github.com/newrelic/go-agent/v3/newrelic"
	"github.com/sirupsen/logrus"
)

type ContextFormatter struct {
}

func (c ContextFormatter) Format(e *logrus.Entry) ([]byte, error) {
	data := make(map[string]interface{}, len(e.Data)+12)
	for k, v := range e.Data {
		data[k] = v
	}
	data[logcontext.KeyTimestamp] = uint64(e.Time.UnixNano()) / uint64(1000*1000)
	data[logcontext.KeyMessage] = e.Message
	data[logcontext.KeyLevel] = e.Level

	data[logcontext.KeyFile] = e.Caller.File
	data[logcontext.KeyLine] = e.Caller.Line
	data[logcontext.KeyMethod] = e.Caller.Function

	if ctx := e.Context; ctx != nil {
		if txn := newrelic.FromContext(ctx); nil != txn {
			logcontext.AddLinkingMetadata(data, txn.GetLinkingMetadata())
		}
	}

	e.Data = data

	l := &logrus.TextFormatter{}

	return l.Format(e)
}
