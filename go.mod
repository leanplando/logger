module bitbucket.org/leanplando/logger

go 1.14

require (
	github.com/google/go-cmp v0.3.1 // indirect
	github.com/kr/pretty v0.1.0 // indirect
	github.com/newrelic/go-agent/v3 v3.11.0 // indirect
	github.com/newrelic/go-agent/v3/integrations/logcontext/nrlogrusplugin v1.0.0
	github.com/sirupsen/logrus v1.8.1
	github.com/stretchr/testify v1.4.0 // indirect
	golang.org/x/net v0.0.0-20190813141303-74dc4d7220e7 // indirect
	golang.org/x/sys v0.0.0-20191220142924-d4481acd189f // indirect
	golang.org/x/text v0.3.2 // indirect
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
)
